// TASK 1

function Product (name, price, discount) {
  this.name = name;
  this.price = price;
  this.discount = discount;
}

Product.prototype.applyDiscount = function () {
  if(this.discount === 100) {
    console.log(`Congrats! You've been chosen to receive ${this.name} for free!`)
    return;
  }
  console.log(`The price for ${this.name} is ${(this.price = this.price * ((100 - this.discount) * 0.01)).toFixed(0)} including ${this.discount} percent discount`);

}

const apple = new Product('apple', '25', 20);

apple.applyDiscount();

// TASK 2

function greeting(person) {
  return `Привіт, я ${person.name}, мені ${person.age} років`;
}

let name,age;

do {
  name = prompt('Please, enter your name');
} while (!name)

do {
  age = +prompt('Please, enter your age');
} while (isNaN(age) || !age)

let person = {
  name: name,
  age: age
};

let message = greeting(person);

alert(message);









